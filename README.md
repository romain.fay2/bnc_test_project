# BNC_test_project

Test project for BNC interview

## Getting started

Create a virtual environment for the test project and activate it.
Clone the repository locally.
With the requirements.txt available at the root of the repository, install the dependencies.
You can now start the server using
```
python manage.py runserver
```
Or by using the script in /bin.
Note that the port may vary, replace 8000 by the one shown by your machine:
```
Django version 4.0.2, using settings 'bnc_test.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

## Querying the API

Once the server is started you can query it using the curl examples given in curl_requests.txt with the correct port.

## Test

To run the test, stop the server (CTRL+C) and run
```
python manage.py test
```
Tests can be found under bnc_test/mind_map/tests .

## Project requirements

The service is an API in Python using Django Rest Framework.
It has tests, provide a local DB and a save in files (leaves.json and mind_maps.json)
You can, using the curl provided document:
- Create a mind map
- Get a single mind map
- Get a list of mind maps
- Delete a mind maps (cascading on leaves)
- Create a leaf
- Get a single leaf related to a map
- Get all leaves related to a map
- Delete a leaf and all it's related leaves
- Extract all data in 2 json
- Create a .txt file (pretty_tree.txt) with a visual representation of a tree

## Code logic

The code I wrote is located in several files in /mind_map:
- models.py
- serializers.py
- tests.py
- urls.py
- views.py