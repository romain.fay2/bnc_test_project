from django.urls import include, path

urlpatterns = [
    path('', include('mind_map.urls')),
]
