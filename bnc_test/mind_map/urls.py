from django.contrib import admin
from django.urls import include, path
from mind_map.views import (MindMapList, MindMapDetail, LeafList, LeafDetail,
                            LeafCreate, DataSaving, PrettyPrintTree)

urlpatterns = [
    # Mind map
    path('mind-maps/', MindMapList.as_view(), name='mind-maps-list'),
    path('mind-maps/<str:id>', MindMapDetail.as_view(), name='mind-map-detail'),
    # Leaf
    path('mind-maps/<str:id>/leaves/', LeafList.as_view(), name='leaves-list'),
    path('leaves/create/', LeafCreate.as_view(), name='leaves-create'),
    path('leaves/', LeafDetail.as_view(), name='leaf-detail'),
    # Others
    path('data_saving/', DataSaving.as_view(), name='data-saving'),
    path('pretty_print_tree/<str:id>',
         PrettyPrintTree.as_view(), name='print-tree'),
]
