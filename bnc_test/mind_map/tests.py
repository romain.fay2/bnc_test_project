import json
from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from mind_map.models import (MindMap, Leaf)


class MindMapTests(APITestCase):
    def test_create_mind_map(self):
        """
        Ensure we can create a new mind map object.
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MindMap.objects.count(), 1)
        self.assertEqual(MindMap.objects.get().id, 'my-test-map')

    def test_create_duplicate_mind_map(self):
        """
        Ensure we can create only a unique id for mind map object.
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MindMap.objects.count(), 1)
        self.assertEqual(MindMap.objects.get().id, 'my-test-map')

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(MindMap.objects.count(), 1)
        self.assertEqual(MindMap.objects.get().id, 'my-test-map')

    def test_get_mind_map(self):
        """
        Ensure we can retrieve a mind map
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/mind-maps/my-test-map')
        self.assertEqual(response.data, {'id': 'my-test-map'})

    def test_get_mind_map_list(self):
        """
        Ensure we can retrieve a list of mind maps
        """
        response = self.client.get('/mind-maps/')
        self.assertEqual(
            response.data, [])

        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map_1'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {'id': 'my-test-map_2'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/mind-maps/')
        self.assertEqual(
            response.data, [{'id': 'my-test-map_1'}, {'id': 'my-test-map_2'}])

    def test_delete_mind_map(self):
        """
        Ensure we can delete a mind map
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete('/mind-maps/my-test-map')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class LeafTests(APITestCase):
    """
    """

    def test_create_leaf(self):
        """
        Ensure we can create a new leaf object.
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Leaf.objects.count(), 1)
        self.assertEqual(Leaf.objects.get().text, 'x')

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Leaf.objects.count(), 2)

    def test_create_duplicate_leaf(self):
        """
        Ensure we can create only a unique leaf object.
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Leaf.objects.count(), 2)

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 2)

    def test_create_incorrect_leaf_slash(self):
        """
        Ensure we create leaf with no / in the path
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "test/test",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 0)

    def test_create_incorrect_leaf_no_root(self):
        """
        Ensure we can't create a non-root leaf if map has no root
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "test",
            "is_root": False,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 0)

    def test_create_incorrect_leaf_two_roots(self):
        """
        Ensure we can't create a root leaf if map has a root
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "test",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Leaf.objects.count(), 1)

        data = {
            "path": "",
            "text": "test2",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 1)

    def test_create_incorrect_leaf_root_path(self):
        """
        Ensure we can't create a leaf with a path if it's root
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "test",
            "text": "test",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 0)

    def test_create_incorrect_leaf_no_parent(self):
        """
        Ensure we can't create a non root leaf if path doesn't match parent
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "test",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Leaf.objects.count(), 1)

        data = {
            "path": "wrong_path",
            "text": "test2",
            "is_root": False,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Leaf.objects.count(), 1)

    def test_get_leaf(self):
        """
        Ensure we can retrieve a leaf
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "",
            "text": "x",
            "mind_map_id": "my-test-map"
        }
        response = self.client.post('/leaves/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_leaf_list(self):
        """
        Ensure we can retrieve a list of leaves
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "z",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/mind-maps/my-test-map/leaves/')
        self.assertEqual(
            len(response.data), 3)

    def test_delete_leaf(self):
        """
        Ensure we can delete a leaf
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "",
            "text": "x",
            "mind_map_id": "my-test-map"
        }
        response = self.client.delete('/leaves/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Leaf.objects.count(), 0)

    def test_cascade_delete_leaf(self):
        """
        Ensure we can delete a leaf with all children leaf
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "z",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x/z",
            "text": "w",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/mind-maps/my-test-map/leaves/')
        self.assertEqual(
            len(response.data), 4)

        data = {
            "path": "x",
            "text": "z",
            "mind_map_id": "my-test-map"
        }
        response = self.client.delete('/leaves/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get('/mind-maps/my-test-map/leaves/')
        self.assertEqual(
            len(response.data), 2)

    def test_cascade_delete_tree(self):
        """
        Ensure we can delete a map mind with all children leaf
        """
        url = reverse('mind-maps-list')
        data = {'id': 'my-test-map'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('leaves-create')
        data = {
            "path": "",
            "text": "x",
            "is_root": True,
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "y",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x",
            "text": "z",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            "path": "x/z",
            "text": "w",
            "mind_map": "my-test-map"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/mind-maps/my-test-map/leaves/')
        self.assertEqual(
            len(response.data), 4)

        response = self.client.delete('/mind-maps/my-test-map')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertEqual(Leaf.objects.count(), 0)
