from email.policy import HTTP
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
import json

from .models import (MindMap, Leaf)
from .serializers import (MindMapSerializer, LeafSerializer,
                          Create_LeafSerializer, Full_LeafSerializer)


class MindMapList(APIView):
    """
    List all mind maps, or create a new mind map.
    """

    def get(self, request, format=None):
        mind_maps = MindMap.objects.all()
        serializer = MindMapSerializer(mind_maps, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MindMapSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MindMapDetail(APIView):
    """
    Retrieve or delete a mind map instance.
    """

    def get_object(self, id):
        try:
            return MindMap.objects.get(id=id)
        except MindMap.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        mind_map = self.get_object(id)
        serializer = MindMapSerializer(mind_map)
        return Response(serializer.data)

    def delete(self, request, id, format=None):
        mind_map = self.get_object(id)
        mind_map.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LeafList(APIView):
    """
    List all leaves of a map.
    """

    def get(self, request, id, format=None):
        leaves = Leaf.objects.all().filter(mind_map=id)
        serializer = LeafSerializer(leaves, many=True)
        return Response(serializer.data)


class LeafCreate(APIView):
    """
    Create a new leaf.
    """

    def post(self, request, format=None):
        serializer = Create_LeafSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeafDetail(APIView):
    """
    Retrieve (post) or delete a Leaf instance.
    """

    def get_object(self, mind_map_id, path, text):
        try:
            return Leaf.objects.get(mind_map=mind_map_id, path=path, text=text)
        except Leaf.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        try:
            leaf = self.get_object(
                request.data['mind_map_id'], request.data['path'], request.data['text'])
            serializer = LeafSerializer(leaf)
            return Response(serializer.data)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        try:
            leaf = self.get_object(
                request.data['mind_map_id'], request.data['path'], request.data['text'])
            leaf.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class DataSaving(APIView):
    """
    Save data in a file as JSON for persistence
    """

    def write_data(self, path_to_file, data):
        """
        """
        f = open(path_to_file, "w")
        f.write(data)
        f.close()

    def get(self, request):
        """
        """
        mind_maps = MindMap.objects.all()
        mind_maps_serializer = json.dumps(
            MindMapSerializer(mind_maps, many=True).data)

        leaves = Leaf.objects.all()
        leaves_serializer = json.dumps(
            Full_LeafSerializer(leaves, many=True).data)

        self.write_data("mind_maps.json", mind_maps_serializer)
        self.write_data("leaves.json", leaves_serializer)

        return Response(status=status.HTTP_200_OK)


class PrettyPrintTree(APIView):
    """
    Write a visual "tree" in a txt file for a mind map.
    """

    def get(self, request, id):
        """
        """
        mind_map = MindMap.objects.get(id=id)
        leaves = Leaf.objects.all().filter(mind_map=mind_map)
        with open('pretty_tree.txt', 'w') as f:
            f = open('pretty_tree.txt', "w")
            f.write("root/\n")
            # if we have leaves
            if len(leaves) > 0:
                tab_cpt = 1
                curr_path = ""
                # we start the writing
                self.write_tree(f, tab_cpt, leaves, curr_path)
            f.close()
        return Response(status=status.HTTP_200_OK)

    def write_tree(self, f, tab_cpt, leaves, curr_path):
        """
        f: file to write tree
        tab_cpt: int to get the number of needed tab
        leaves: list of leaves of a tree
        curr_path: current path being trated
        """
        for leaf in leaves.filter(path=curr_path):
            new_line = tab_cpt*"\t" + leaf.text + "/\n"
            f.write(new_line)
            # if this leaf has children, treat them before going to the next
            self.write_tree(f, tab_cpt+1, leaves, leaf.next_leaf_path)
