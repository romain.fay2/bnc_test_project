from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from .models import (MindMap, Leaf)


class MindMapSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = MindMap
        fields = ['id']


class LeafSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = Leaf
        fields = ['id', 'path', 'text']


class Full_LeafSerializer(serializers.ModelSerializer):
    """
    """
    mind_map = MindMapSerializer(read_only=True)
    previous_leaf = LeafSerializer(read_only=True)

    class Meta:
        model = Leaf
        fields = ['id', 'path', 'text', 'is_root', 'mind_map', 'previous_leaf',
                  'next_leaf_path']


class Create_LeafSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = Leaf
        fields = ['path', 'text', 'is_root', 'mind_map', 'previous_leaf']

    validators = [
        # The uniqueness of a leaf is (path, text, mind_map)
        UniqueTogetherValidator(
            queryset=Leaf.objects.all(),
            fields=['path', 'text', 'mind_map']
        )
    ]

    def validate(self, data):
        tree_leaves = Leaf.objects.all().filter(mind_map=data['mind_map'])
        if "/" in data['text']:
            raise serializers.ValidationError(
                'You cannot include / char in the text.')
        # if root is specified and true
        if ('is_root' in data) and (data['is_root']):
            if len(tree_leaves) > 0:
                raise serializers.ValidationError(
                    'This map already has a root')
            if data['path'] != "":
                raise serializers.ValidationError(
                    'Root leaf path must be empty')
        else:
            if len(tree_leaves) == 0:
                raise serializers.ValidationError(
                    'This map has no root')
            if len(tree_leaves.filter(next_leaf_path=data['path'])) == 0:
                raise serializers.ValidationError(
                    'This leaf path does not stick to an existing one')
        return data
