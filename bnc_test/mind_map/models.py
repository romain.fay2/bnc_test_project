from django.db import models
from django.utils.text import slugify


class MindMap(models.Model):
    """
    Mindmap object
        id: name of the mindmap, unique
    """
    id = models.CharField(max_length=255, unique=True, primary_key=True)

    def __str__(self):
        return self.id


class Leaf(models.Model):
    """
    Leaf object
        path: path leading to leaf, unique
        text: text of the leaf
        is_root: only true for the starting leaf of the mind map
        mind_map: mind map of the leaf
        previous_leaf: link to the previous leaf if existing
        next_leaf_path: path of the children leaves, used for validation
    """
    path = models.TextField(null=True, blank=True)
    text = models.CharField(max_length=255)
    is_root = models.BooleanField(default=False)
    mind_map = models.ForeignKey(MindMap, on_delete=models.CASCADE)
    previous_leaf = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.CASCADE)
    next_leaf_path = models.TextField()

    def save(self, *args, **kwargs):
        if self.is_root:
            self.next_leaf_path = self.text
        else:
            # to get next leaf path, append new leaf text
            self.next_leaf_path = '/'.join([self.path, self.text])
            # to get previous leaf, remove last leaf of current leaf path
            self.previous_leaf = Leaf.objects.get(
                mind_map=self.mind_map,
                path='/'.join(self.path.split("/")[:-1]),
                text=self.path.split("/")[-1])
        super(Leaf, self).save(*args, **kwargs)
